import { DocumentModificationOptions } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs';
import BaseUser from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/documents/user.mjs';
import { Updates } from '../../../globals';
import { reshuffleActionDeck } from '../../util';

import { AmbushAssistant } from '../../apps/AmbushAssistant';
import { CardPickResult, CardPicker } from '../../apps/CardPicker';
import { PlayerCardDrawHerder } from '../../apps/PlayerCardDrawHerder';
import SwadeUser from '../SwadeUser';
import type SwadeActiveEffect from '../active-effect/SwadeActiveEffect';
import SwadeCards from '../card/SwadeCards';
import SwadeCombatant from './SwadeCombatant';

declare global {
  interface DocumentClassConfig {
    Combat: typeof SwadeCombat;
  }
}

export default class SwadeCombat extends Combat {
  /** an internal helper flag that's being checked to see if we're currently asking to advance the round */
  #roundAdvanceDialog: boolean = false;

  /** Compares two combatants by name. */
  static nameSortCombatants(a: SwadeCombatant, b: SwadeCombatant): number {
    if (a.name === b.name) return SwadeCombat.#idSortCombatants(a, b);
    return a.name > b.name ? 1 : -1;
  }

  /** Compares two combatants by ID. */
  static #idSortCombatants(a: SwadeCombatant, b: SwadeCombatant): number {
    return a.id! > b.id! ? 1 : -1;
  }

  static INITIATIVE_SOUND = 'systems/swade/assets/card-flip.wav';

  get actionDeck(): SwadeCards {
    return game.cards!.get(game.settings.get('swade', 'actionDeck'), {
      strict: true,
    });
  }

  get automaticInitiative(): boolean {
    return game.settings.get('swade', 'autoInit');
  }

  #debouncedCombatSound: this['_playCombatSound'];

  #initSoundData: AudioHelper.PlayData = {
    src: SwadeCombat.INITIATIVE_SOUND,
    volume: 0.8,
    autoplay: true,
    loop: false,
  };

  constructor(...args) {
    super(...args);
    this.#debouncedCombatSound = foundry.utils.debounce(
      super._playCombatSound,
      200,
    );
  }

  override async rollInitiative(
    ids: string | string[],
    { messageOptions, updateTurn }: Combat.InitiativeOptions = {},
  ) {
    // Structure input data
    ids = Array.isArray(ids) ? ids : [ids];

    const currentId = this.combatant?.id;
    const messages: foundry.documents.BaseChatMessage.ConstructorData[] = [];
    const updates: Updates[] = [];

    //Check if enough cards are available
    if (ids.length > this.actionDeck.availableCards.length) {
      const message = game.i18n.format('SWADE.NoCardsLeft', {
        needed: ids.length,
        current: this.actionDeck.availableCards.length,
      });
      ui.notifications.warn(message);
      return this;
    }
    // Iterate over Combatants, performing an initiative draw for each
    for (const id of ids) {
      // Get Combatant data
      const c = this.combatants.get(id, { strict: true }) as SwadeCombatant;
      if (!c.isOwner) continue;
      const roundHeld = !!c.roundHeld;
      //Do not draw cards for defeated, holding or grouped combatants
      if (c.isDefeated || roundHeld || !!c.groupId || c.turnLost) continue;

      // Set up edges
      const hasHesitant = c.actor?.system.initiative.hasHesitant;
      const hasQuick = c.actor?.system.initiative.hasQuick;
      const isIncapacitated = c.isIncapacitated;

      // Figure out how many cards to draw
      const cardsToDraw = c.cardsToDraw;

      // Draw initiative
      let pickedCard: Card;
      let cardsToPickFrom = await this.drawCard(cardsToDraw);
      const isRedraw = typeof c.initiative === 'number' && !roundHeld;

      if (isRedraw) {
        // handle redraws
        const oldCard = this.findCard(c?.cardValue!, c?.suitValue!);
        if (oldCard) {
          cardsToPickFrom.push(oldCard);
          const result = await this.pickACard({
            cards: cardsToPickFrom,
            combatantName: c.name,
            oldCardId: oldCard?.id!,
          });
          pickedCard = result.picked;
          cardsToPickFrom = result.cards;
        } else {
          pickedCard = cardsToPickFrom[0];
        }
      } else if (isIncapacitated) {
        pickedCard = cardsToPickFrom[0];
      } else if (hasHesitant) {
        // Hesitant
        const joker = cardsToPickFrom.find((c) => c.system['isJoker']);
        if (joker) {
          // if one of the cards drawn was a joker, simply use that
          pickedCard = joker;
        } else {
          //sort cards to pick the lower one
          cardsToPickFrom.sort((a, b) => {
            const cardA = a.value!;
            const cardB = b.value!;
            const card = cardA - cardB;
            if (card !== 0) return card;
            const suitA = a.system['suit'];
            const suitB = b.system['suit'];
            const suit = suitA - suitB;
            return suit;
          });
          pickedCard = cardsToPickFrom[0];
        }
      } else if (cardsToDraw > 1) {
        //Level Headed
        const result = await this.pickACard({
          cards: cardsToPickFrom,
          combatantName: c.name,
          enableRedraw: hasQuick,
          isQuickDraw: hasQuick,
        });
        pickedCard = result.picked;
        cardsToPickFrom = result.cards;
      } else if (hasQuick) {
        pickedCard = cardsToPickFrom[0];
        const cardValue = pickedCard.value!;
        //if the card value is less than 5 then pick a card otherwise use the card
        if (cardValue <= 5) {
          const result = await this.pickACard({
            cards: [pickedCard],
            combatantName: c.name,
            enableRedraw: true,
            isQuickDraw: true,
          });
          pickedCard = result.picked;
          cardsToPickFrom = result.cards;
        }
      } else {
        //normal card draw
        pickedCard = cardsToPickFrom[0];
      }
      const newFlags = {
        cardValue: pickedCard.value!,
        suitValue: pickedCard.system['suit'],
        hasJoker: pickedCard.system['isJoker'],
        cardString: pickedCard.description,
      };

      const initiative =
        (pickedCard.value as number) +
        (pickedCard?.system['suit'] as number) / 10;

      const update = {
        _id: id,
        initiative,
        flags: { swade: newFlags },
      };

      //Handle group leader changes
      updates.push(update);

      //handle potential followers
      let fInitiative = initiative;
      for (const f of c.followers) {
        updates.push({
          _id: f.id,
          initiative: (fInitiative -= 0.001),
          'flags.swade': newFlags,
        });
      }

      // Construct chat message data
      const messageData = foundry.utils.mergeObject(
        {
          speaker: ChatMessage.getSpeaker({
            actor: c.actor,
            token: c.token,
            alias: c.name,
          }),
          whisper:
            c.token?.hidden || c.hidden
              ? game?.users?.filter((u) => u.isGM)
              : [],
          content: '', //keep the content empty so we don't trigger validation warnings
          flags: {
            swade: {
              isRedraw,
              pickedCard: pickedCard.id,
              cards: cardsToPickFrom.map((c) => c.toObject()),
            },
          },
        },
        messageOptions,
      );
      messages.push(messageData);
    }

    if (!updates.length) return this;

    // Update the combat instance with the new combatants
    await this.updateEmbeddedDocuments('Combatant', updates);

    // Create multiple chat messages
    this._playInitiativeSound();
    await getDocumentClass('ChatMessage').createDocuments(messages);

    const activeCombatants = this.combatants.filter((c) => !c.isDefeated);
    if (activeCombatants.every((c) => !!c.initiative)) {
      await this.update({ turn: 0 });
      this._handleStartOfTurnExpirations();
    } else if (updateTurn && currentId) {
      // Ensure the turn order remains with the same combatant
      await this.update({
        turn: this.turns.findIndex((t) => t.id === currentId),
      });
    }

    // Return the updated Combat
    return this;
  }

  protected override _sortCombatants(
    a: SwadeCombatant,
    b: SwadeCombatant,
  ): number {
    const currentRound = game.combat?.round ?? 0;

    if (
      (a.roundHeld && currentRound !== a.roundHeld) ||
      (b.roundHeld && currentRound !== b.roundHeld)
    ) {
      const isOnHoldA = a.roundHeld && (a.roundHeld ?? 0 < currentRound);
      const isOnHoldB = b.roundHeld && (b.roundHeld ?? 0 < currentRound);
      if (isOnHoldA && !isOnHoldB) return -1;
      if (!isOnHoldA && isOnHoldB) return 1;
    }
    if (b.initiative === a.initiative) {
      return SwadeCombat.nameSortCombatants(a, b);
    } else {
      return b.initiative - a.initiative;
    }
  }

  /**
   * Draws cards from the Action Cards deck
   * @param count number of cards to draw
   * @returns an array with the drawn cards
   */
  async drawCard(count = 1): Promise<Card[]> {
    const pileId = game.settings.get('swade', 'actionDeckDiscardPile');
    const discardPile = game.cards!.get(pileId, { strict: true });
    return this.actionDeck.dealForInitiative(discardPile, count);
  }

  /** Ask the user to pick a card for a given combatant name */
  async pickACard(ctx: CardPickContext): Promise<CardPickResult> {
    return CardPicker.asPromise({ ...ctx, deck: this.actionDeck });
  }

  /**
   * Find a card from the deck based on it's suit and value
   * @param cardValue
   * @param cardSuit
   */
  findCard(cardValue: number, cardSuit: number): Card | undefined {
    return this.actionDeck.cards.find(
      (c) =>
        c.type === 'poker' &&
        c.value === cardValue &&
        c.system['suit'] === cardSuit,
    );
  }

  override async resetAll() {
    for (const combatant of this.combatants) {
      const update = this._getInitResetUpdate(combatant as SwadeCombatant);
      if (update) combatant.updateSource(update);
    }
    await this.update(
      { turn: 0, combatants: this.combatants.toObject() },
      { diff: false },
    );
    return this;
  }

  override async startCombat() {
    //Init autoroll
    if (this.automaticInitiative) {
      // if automatic init is on we draw cards
      await this._promptAllPlayersForInitiative();
      //grab the NPCs, we're drawing them locally
      await this.rollNPC();
    }
    return super.startCombat();
  }

  startSurpriseCombat() {
    new AmbushAssistant(this).render(true);
  }

  override async nextTurn() {
    await this._handleEndOfTurnExpirations();
    const turn = this.turn ?? -1;

    // Determine the next turn number
    let next: number | null = null;
    if (this.settings.skipDefeated) {
      for (const [i, t] of this.turns.entries()) {
        if (i <= turn) continue;
        // Skip defeated, lost turns
        if (t.isDefeated || t.turnLost) continue;
        next = i;
        break;
      }
    } else {
      next = turn + 1;
    }

    // Maybe advance to the next round
    const round = this.round;
    if (this.round === 0 || next === null || next >= this.turns.length) {
      return this.nextRound();
    }

    // Update the document, passing data through a hook first
    const updateData = { round, turn: next };
    const updateOptions = { advanceTime: CONFIG.time.turnTime, direction: 1 };
    Hooks.callAll('combatTurn', this, updateData, updateOptions);
    await this.update(updateData, updateOptions);
    await this._handleStartOfTurnExpirations();
    return this;
  }

  override async nextRound() {
    if (game.user?.isGM) await this._nextRoundAsGM();
    else await this._nextRoundAsUser();
    return this;
  }

  override async previousRound() {
    const revert = await Dialog.confirm({
      title: game.i18n.localize('SWADE.Combat.RevertRoundTitle'),
      content:
        '<p>' + game.i18n.localize('SWADE.Combat.RevertRoundContent') + '</p>',
      defaultYes: true,
      rejectClose: false,
      options: { classes: [...Dialog.defaultOptions.classes, 'swade-app'] },
    });
    if (!revert) return this;
    return super.previousRound();
  }

  protected _getInitResetUpdate(
    combatant: SwadeCombatant,
  ): Record<string, unknown> | undefined {
    const roundHeld = combatant.roundHeld;
    const turnLost = combatant.turnLost;
    const groupId = combatant.groupId;
    if (roundHeld) {
      if (turnLost && groupId) {
        return {
          initiative: null,
          'flags.swade': {
            hasJoker: false,
            '-=turnLost': null,
          },
        };
      } else {
        //keep the card
        return;
      }
    } else if (!roundHeld || turnLost) {
      return {
        initiative: null,
        'flags.swade': {
          suitValue: null,
          cardValue: null,
          hasJoker: false,
          cardString: '',
          turnLost: false,
        },
      };
    }
    return {
      initiative: null,
      'flags.swade': {
        suitValue: null,
        cardValue: null,
        hasJoker: false,
        cardString: '',
        turnLost: false,
      },
    };
  }

  protected async _handleStartOfTurnExpirations() {
    if (this.combatant.isDefeated) return;
    const expirations =
      this.combatant?.actor?.effects.filter(
        (effect: SwadeActiveEffect) =>
          effect.isTemporary && effect.isExpired('start'),
      ) ?? [];
    for (const effect of expirations) {
      await effect.expire();
    }
  }

  protected async _handleEndOfTurnExpirations() {
    if (this.combatant.isDefeated) return;
    const expirations =
      this.combatant?.actor?.effects.filter(
        (effect) => effect.isTemporary && effect.isExpired('end'),
      ) ?? [];
    for (const effect of expirations) {
      await effect.expire();
    }
  }

  protected async _playInitiativeSound() {
    if (!game.settings.get('swade', 'initiativeSound')) return;
    AudioHelper.play(this.#initSoundData, true);
  }

  protected override _playCombatSound(type: string): void {
    if (this.turn === this.turns.length - 1 && type === 'nextUp') return; //skip if it's the last turn in the round
    this.#debouncedCombatSound(type);
  }

  protected async _nextRoundAsGM() {
    if (this.#roundAdvanceDialog) return;
    this.#roundAdvanceDialog = true; //set the flag
    //run the dialog
    const advance = await Dialog.confirm({
      title: game.i18n.localize('SWADE.Combat.AdvanceRoundTitle'),
      content:
        '<p>' + game.i18n.localize('SWADE.Combat.AdvanceRoundContent') + '</p>',
      defaultYes: true,
      rejectClose: false,
      options: { classes: [...Dialog.defaultOptions.classes, 'swade-app'] },
    });
    this.#roundAdvanceDialog = false; //unset the flag
    if (!advance) return;
    //reset the deck if a joker had been drawn
    if (this.combatants.some((c: SwadeCombatant) => c.hasJoker)) {
      await reshuffleActionDeck();
      ui.notifications.info('SWADE.DeckShuffled', { localize: true });
    }

    //reset the combatants
    await this.resetAll();

    //advance the round to the next one
    await super.nextRound();

    //no auto init, we're done;
    if (!this.automaticInitiative) return;

    // if automatic init is on we draw cards
    await this._promptAllPlayersForInitiative();
    //grab the NPCs, we're drawing them locally
    await this.rollNPC();
  }

  /** As a user emit a socket event that asks a Game master to trigger the next round workflow and roll the owned tokens */
  protected _nextRoundAsUser() {
    game.swade.sockets.newRound(this.id!);
  }

  protected async _promptAllPlayersForInitiative() {
    const [localDraws, remoteDraws] = this.combatants
      .filter(
        (c: SwadeCombatant) =>
          c.hasPlayerOwner && !c.isNPC && c.initiative === null,
      )
      .map((c: SwadeCombatant) => {
        return { combatant: c, user: c.players[0] as SwadeUser };
      })
      .sort((a, b) => a.user.name.localeCompare(b.user.name))
      .partition((v) => this._determineIfRemoteDraw(v.user, v.combatant));

    for (const { combatant } of localDraws) {
      await this.rollInitiative(combatant.id as string);
    }
    if (remoteDraws.length > 0) {
      await PlayerCardDrawHerder.asPromise({
        draws: remoteDraws,
        combatId: this.id as string,
      });
    }
  }

  protected _determineIfRemoteDraw(
    user: SwadeUser,
    combatant: SwadeCombatant,
  ): boolean {
    const initiative = combatant.actor?.system.initiative;
    const edges =
      initiative.hasLevelHeaded ||
      initiative.hasImpLevelHeaded ||
      initiative.hasQuick;
    return user.active && edges;
  }

  override async _preDelete(
    options: DocumentModificationOptions,
    user: BaseUser,
  ) {
    await super._preDelete(options, user);
    const jokerDrawn = this.combatants.some((c: SwadeCombatant) => c.hasJoker);

    //reset the deck when combat is ended
    if (jokerDrawn) {
      await reshuffleActionDeck();
      ui.notifications.info('SWADE.DeckShuffled', { localize: true });
    }

    //remove the holding status from any combatants that have it
    await Promise.allSettled(
      this.combatants
        .filter((c) => c.actor?.statuses.has('holding'))
        .flatMap((c) =>
          c.actor?.effects.filter((e) => e.statuses.has('holding')),
        )
        .map((e) => e.delete()),
    );
  }
}

interface CardPickContext {
  /** an array of cards */
  cards: Card[];
  /** name of the combatant */
  combatantName: string;
  /** id of the old card, if you're picking cards for a redraw */
  oldCardId?: string;
  /** determines whether a redraw is allowed */
  enableRedraw?: boolean;
  /** determines whether this draw includes the Quick edge */
  isQuickDraw?: boolean;
}
